from bot.handlers import handler
from bot.handlers.client import main as client_main
from bot.handlers.worker import main as worker_main


routers = [
    handler.router,
    client_main.router,
    worker_main.router
]
